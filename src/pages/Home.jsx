import React, { Component } from 'react'
import Axios from 'axios';
import PostCard from '../components/PostCard';

export class Home extends Component {
    constructor() {
        super();
        this.state = {
            posts: []
        }
    }

    async componentDidMount() {
        try {
            let { data: posts } = await Axios.get('http://localhost:5000/api/')

            this.setState({
                posts
            })
        } catch (error) {
            // toast
            console.log(error)
        }
    }

    render() {
        const { posts } = this.state;
        return (
            <div>
                <h1>Home</h1>
                <hr></hr>

                {
                    posts.map(post => <PostCard key={post._id} post={post} />)
                }
            </div>
        )
    }
}

export default Home
