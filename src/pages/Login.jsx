import React, { Component } from 'react'
import Axios from 'axios'
import { toast } from 'react-toastify';

export class Login extends Component {
    state = {
        email: '',
        password: '',
        validationError: null
    }

    login = async (e) => {
        e.preventDefault();
        let { email, password } = this.state;

        // validation logic

        try {
            let response = await Axios.post('http://localhost:5000/api/login', {
                email,
                password
            });
            console.log(response);
            const { data: { token } } = response;
            localStorage.setItem('token', token);
            this.props.login();
        } catch (error) {
            // validation by status code
            // console.log(error.response);
            if (error.response) {
                toast.error(error.response.data.message);
            } else if (error.request) {
                toast('Check your network connection')
            } else {
                console.log(error);
                toast('Something went wrong')
            }

        }
    }

    onChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        // console.log(this.state);
        let { email, password } = this.state;
        return (
            <div className="container">
                <h1>Login</h1>
                <form>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input
                            type="email"
                            className="form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter email"
                            name="email"
                            value={email}
                            onChange={this.onChangeHandler}
                        />
                        <small>validation error</small>
                        <small id="emailHelp" className="form-text text-muted">We'll
                        <small>validation error</small> never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input
                            type="password"
                            className="form-control"
                            id="exampleInputPassword1"
                            placeholder="Password"
                            name="password"
                            value={password}
                            onChange={this.onChangeHandler}
                        />
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={this.login}>Submit</button>
                </form>
            </div>
        )
    }
}

export default Login
