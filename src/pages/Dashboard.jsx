import React, { Component } from 'react'
import PostTable from '../components/PostTable'
import Axios from 'axios';
import PostModal from '../components/PostModal';
import JWTDecode from 'jwt-decode';

export class Dashboard extends Component {
    state = {
        posts: [],
        show: false,
        idToBeUpdate: null,
        modalFlag: null
    };

    async componentDidMount() {
        try {
            const user = JWTDecode(localStorage.getItem('token'));

            let { data: posts } = await Axios.get(`http://localhost:5000/api/?userId=${user._id}`);
            this.setState({
                posts
            })
        } catch (error) {
            console.log(error);
        }
    }

    handleClose = () => this.setState({ show: false });
    handleShow = (flag, id = null) => {
        // id = id || null;
        this.setState({
            show: true,
            idToBeUpdate: id,
            modalFlag: flag
        });
    }

    editPost = async ({ title, body, idToBeUpdate }) => {
        // api req
        try {
            // header append
            let response = await Axios.patch(`http://localhost:5000/api/${idToBeUpdate}`,
                {
                    title,
                    body
                },
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')

                    }
                }
            )
            // console.log(response);

            //  state change
            // all state
            let posts = this.state.posts;

            //edit
            posts = posts.map(post => {
                if (post._id !== idToBeUpdate) return post;

                post.title = title;
                post.body = body;
                return post;
            })

            //setState
            this.setState({
                posts
            })

            this.handleClose();
        } catch (error) {
            console.log(error);
        }

    }

    addPost = async ({ title, body }) => {
        try {
            const response = await Axios.post('http://localhost:5000/api/',
                { title, body },
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')

                    }
                }
            )

            this.setState({
                posts: [response.data.newPost, ...this.state.posts]
            });

            this.handleClose();
        } catch (error) {
            console.log(error);
        }
    }

    addPost = async ({ title, body }) => {
        try {
            const response = await Axios.post('http://localhost:5000/api/',
                { title, body },
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')

                    }
                }
            )

            this.setState({
                posts: [response.data.newPost, ...this.state.posts]
            });

            this.handleClose();
        } catch (error) {
            console.log(error);
        }
    }

    deletePost = async (id) => {
        try {
            const response = await Axios.delete(`http://localhost:5000/api/${id}`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    }
                }
            )

            let posts = this.state.posts.filter(post => post._id !== id);

            this.setState({
                posts
            })

            this.handleClose();
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        const { posts, show, idToBeUpdate, modalFlag } = this.state;
        return (
            <div>
                <h1>Dashboard</h1>
                <hr></hr><br></br>

                <PostTable
                    posts={posts}
                    handleShow={this.handleShow}
                    deletePost={this.deletePost}
                />
                {
                    show ? (<PostModal
                        show={show}
                        handleClose={this.handleClose}
                        idToBeUpdate={idToBeUpdate}
                        editPost={this.editPost}
                        modalFlag={modalFlag}
                        addPost={this.addPost}
                    />) : ''
                }

                {/* <PostModal
                    show={show}
                    handleClose={this.handleClose}
                    idToBeUpdate={idToBeUpdate}
                /> */}

            </div>
        )
    }
}

export default Dashboard
