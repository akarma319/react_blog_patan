import React, { Component } from 'react';
import Navbar from './components/Navbar';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import { toast } from 'react-toastify';
import SinglePost from './pages/SinglePost';

// Call it once in your app. At the root of your app is the best place
toast.configure()

// screen if you're not yet authenticated.
function ProtectedRoute(props) {
  let { children, ...rest } = props;
  return (
    <Route {...rest} render={({ location }) =>
      isAuthenticate() ? (
        children
      ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
    }
    />
  );
}

// localStorage.setItem
// localStorage.getItem

function isAuthenticate() {
  let token = localStorage.getItem('token');

  return token ? true : false;
}

class App extends Component {
  constructor() {
    super();
    this.state = {
      isAuthenticate: isAuthenticate()
    }
  }

  login = () => {
    // login logic
    this.setState({
      isAuthenticate: true
    })
  }

  logout = () => {
    localStorage.clear('token');
    this.setState({
      isAuthenticate: false
    })
  }
  render() {
    let { isAuthenticate } = this.state;
    console.log('isAuthenticate', isAuthenticate)
    return (
      <BrowserRouter>
        <Navbar isAuthenticate={isAuthenticate} logout={this.logout} />
        {/* <h1>Hello world</h1> */}
        <div className="container my-5">
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>

            {/* <Route exact path="/dashboard">
            <Dashboard />
          </Route> */}

            <ProtectedRoute exact path="/dashboard">
              <Dashboard />
            </ProtectedRoute>

            <Route path="/post/:_id">
              <SinglePost />
            </Route>

            <Route exact path="/login">
              {isAuthenticate ?
                <Redirect to="/dashboard" /> : <Login login={this.login} />}
            </Route>
          </Switch>
        </div>
      </BrowserRouter >
    );
  }
}

export default App;
