import React from 'react';
import { Table, Button } from 'react-bootstrap';
import { dateFormatter } from '../helpers/dateFormatter';

export default function PostTable(props) {
    const { posts, handleShow, deletePost } = props;
    console.log(posts)
    return (
        <div>
            <Button variant="primary" onClick={() => handleShow('add')}>
                + Add Post
            </Button><br></br><br></br>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {posts.map((post, index) => {
                        return (
                            <tr key={post._id}>
                                <td>{index + 1}</td>
                                <td>{post.title}</td>
                                <td>{dateFormatter(post.createdAt)}</td>
                                <td>
                                    <Button variant="success" size="sm" onClick={() => handleShow('edit', post._id)}>
                                        Edit
                                    </Button>{' '}
                                    <Button variant="danger" size="sm" onClick={() => deletePost(post._id)}>
                                        Delete
                                    </Button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        </div>
    )
}
