import React, { useEffect, useState } from 'react'
import { Modal, Button, Form } from 'react-bootstrap'
import Axios from 'axios';

export default function PostModal(props) {
    const { show, handleClose, idToBeUpdate, editPost, addPost, modalFlag } = props;

    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        try {
            const fetchPost = async () => {
                setLoading(true);
                let { data: post } = await Axios.get(`http://localhost:5000/api/${idToBeUpdate}`);
                setTitle(post.title);
                setBody(post.body);
                setLoading(false);
            }

            if (idToBeUpdate) fetchPost();
        } catch (error) {
            setLoading(false);
            console.log(error);
        }

        return () => {
            console.log('clean up');
            setTitle('');
            setBody('');
            setLoading(true)
        }
    }, [idToBeUpdate]);

    return (
        <div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {modalFlag === 'edit' ? 'Edit Post' : 'Add Post'}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {loading ? 'Loading...' :
                        (<Form>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Title</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter your blog title..."
                                    value={title}
                                    onChange={e => setTitle(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Body</Form.Label>
                                <Form.Control
                                    type="text"
                                    as="textarea"
                                    placeholder="Start writing your post..."
                                    value={body}
                                    onChange={e => setBody(e.target.value)}
                                />
                            </Form.Group>
                        </Form>)}

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    {
                        modalFlag === 'edit'
                            ? (
                                <Button variant="primary" onClick={e => editPost({ title, body, idToBeUpdate })}>
                                    Save Changes
                                </Button>
                            )
                            : (
                                <Button variant="primary" onClick={e => addPost({ title, body })}>
                                    Save
                                </Button>
                            )
                    }
                </Modal.Footer>
            </Modal>
        </div>
    )
}
