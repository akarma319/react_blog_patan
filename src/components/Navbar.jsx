import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Navbar extends Component {
    render() {
        let { isAuthenticate } = this.props;
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                    <Link className="navbar-brand" to="/">Navbar</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item active">
                                <Link className="nav-link" to="/" >Home</Link>
                            </li>
                            {
                                isAuthenticate
                                    ? (
                                        <li className="nav-item active">
                                            <Link className="nav-link" to="/dashboard">Dashboard</Link>
                                        </li>
                                    ) : ''
                            }
                            <li className="nav-item active">
                                {
                                    isAuthenticate
                                        ? <Link className="nav-link" to="/login" onClick={this.props.logout}>Logout</Link>
                                        : <Link className="nav-link" to="/login">Login</Link>
                                }
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Navbar
